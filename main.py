"""
ADVERTENCIA: Si por alguna razon pyttsx3 comienza a usar una voz en ingles entonces ir a este enlace:
https://stackoverflow.com/questions/38992819/how-to-set-property-age-gender-or-language-in-pytts-python
"""

from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob
import speech_recognition as speech
import os,random,pyttsx3;

train =[
    # Saludo
    ('hola', 'saludo'),
    ('hola como estas', 'saludo'),
    ('hola evelyn', 'saludo'),

    # Temperatura
    ('como esta el clima', 'get_temperature'),
    ('tengo frio', 'get_temperature'),
    ('tengo calor', 'get_temperature'),
    ('clima de', 'get_temperature'),

    # Luz encendida
    ('enciende la luz', 'light_on'),
    ('prende el foco', 'light_on'),
    ('no veo nada', 'light_on'),
    ('ilumina la habitacion', 'light_on'),

    # Apagar luces
    ('apaga la luz', 'light_off'),
    ('apaga el foco', 'light_off'),
    ('fuera luces', 'light_off'),
    ('apagar luces', 'light_off'),
]

test = [
    # Saludo
    ('hola', 'saludo'),
    ('que tal Evelyn', 'saludo'),
    ('hola Evelyn como estas', 'saludo'),

    # Temperatura
    ('siento un poco de frio', 'get_temperature'),
    ('se siente calor', 'get_temperature'),
    ('como esta el clima en mexico', 'get_temperature'),
    ('tengo un poco de frio', 'get_temperature'),

    # Luz encendida
    ('evelyn enciende la luz', 'light_on'),
    ('puedes prender el foco', 'light_on'),
    ('no puedo ver nada', 'light_on'),
    ('oye Evelyn ilumina la habitacion', 'light_on')

]
cl = NaiveBayesClassifier(train)

def random_phrases(typeClass):
  phrases =[
    {"saludo": "¡Hola!, me alegra escucharte"},
    {"saludo": "¡Hola!"},
    {"saludo": "Hola Luis, estoy lista para trabajar"},
    {"get_temperature": "Aqui tienes la temperatura de hoy"},
    {"get_temperature": "No olvides usar paraguas ya que habra un poco de lluvia"},
    {"get_temperature": "Hoy será un día muy frío"},
    {"get_temperature": "Hoy habrá mucho sol, usa bloqueador solar"},
    {"light_on" : "Encendiendo luces"},
    {"light_on" : "Que se haga la luz"},
    {"light_on" : "Muy bien iluminaré el cuarto"},
    {"light_on" : "Recuerda apagar la luz cuando te vayas de la habitacion"},
    {"light_off": "Apagando luces"},
    {"light_off": "Luces apagadas"},
    {"light_off": "Claro. listo ya las he apagado"}
  ]

  # Extraer las llaves clonadas y organizarlo en un nuevo arreglo
  merged = {}
  for d in phrases:
    for k, v in d.items():
      if k not in merged: merged[k] = []
      merged[k].append(v)

  # Calcular cuantos items tiene merged[x]
  merged_len = len(merged[typeClass]) - 1
  return merged[typeClass][random.randint(0,merged_len)]


# Clasificar instruccion enviada por el usuario
def IA(command):
  tag = cl.classify(command)

  tag_content = {"class": tag, "content": command}
  return tag_content

# Reconocimiento de voz
def recognize_speech(recognizer, microphone):

  if not isinstance(recognizer, speech.Recognizer):
    raise TypeError("'recognizer' debe ser instanciado como 'Recognizer'")

  if not isinstance(microphone, speech.Microphone):
    raise TypeError("'microphone' debe ser instanciado como 'Microphone'")

  with microphone as source:
    recognizer.adjust_for_ambient_noise(source)
    audio = recognizer.listen(source)

  response = {
    "success": True,
    "error": None,
    "transcription": None
  }

  try:
    response["transcription"] = recognizer.recognize_google(audio, language="es-Mx")
  except speech.RequestError:
    response["success"] = False
    response["error"] = "API No disponible"
  except speech.UnknownValueError:
    response["error"] = "No ha sido posible reconocer la voz"
  return response


if __name__ == '__main__':
  # Iniializar el engine del tts
  engine = pyttsx3.init();
  # Establecer el numero de intentos
  NUM_INTENTOS = 3
  NUM_RECONEXIONES = 5

  # Crear instancias de regonizer y mic
  recognizer = speech.Recognizer()
  microphone = speech.Microphone()

  for i in range(NUM_INTENTOS):
    # Obtener instruccion del usuario
    # Si la transcription es retornada correctamente romper el loop y enviar al a red neuronal
    # Si no se retorna la transcription y la API ha fallado romper el loop y salir
    # Si la API retorna success pero no hay transcription intentar reconectar hasta obtener uno
    # de los dos resultados posibles 

    for j in range(NUM_RECONEXIONES):
      print("intento: {}. Habla ahora".format(i+1))
      instruction = recognize_speech(recognizer, microphone)
      if instruction["transcription"]:
        break
      if not instruction["success"]:
        break
      print("No he entendido nada, ¿que haz dicho?")

    # Si existe un error detener el programa
    if instruction["error"]:
      print("ERROR: {}".format(instruction["error"]))
      break
    # Enviar transcripcion a la red neuronal para clasificar
    sentence_class = IA(instruction["transcription"].lower())
    
    if sentence_class['class'] == 'saludo':
      print("haz dicho: {}".format(sentence_class["content"]).capitalize())
      phrase = random_phrases('saludo')
      print(phrase)
      engine.say(phrase)
      engine.runAndWait();

    if sentence_class['class'] == "get_temperature":
      print("haz dicho: {}".format(sentence_class["content"]).capitalize())
      phrase = random_phrases('get_temperature')
      print(phrase)
      engine.say(phrase)
      engine.runAndWait();

    if sentence_class['class'] == "light_on":
      print("haz dicho: {}".format(sentence_class["content"]).capitalize())
      phrase = random_phrases('light_on')
      print(phrase)
      engine.say(phrase)
      engine.runAndWait();

    if sentence_class['class'] == "light_off":
      print("haz dicho: {}".format(sentence_class["content"]).capitalize())
      phrase = random_phrases('light_off')
      print(phrase)
      engine.say(phrase)
      engine.runAndWait();


