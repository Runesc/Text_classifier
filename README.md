# EClassifier

EClassifier es una red neuronal con NLP integrado capaz de recibir instrucciones
por voz y convertirla a texto para finalmente clasificar la instruccion y tomar
desisiones en base al tipo de clase que sea por medio de un sistema de keys y 
values los cuales son configurables para diferentes tipos de proyectos, ademas 
cuenta con respuestas aleatorias dependiendo de la clase.

## Requisitos
- nltk 3.3
- textblob 0.15.1
- PyAudio 0.2.11+
- FLAC encoder (Requerido solo si el sistema no esta basado en una arquitectura x86 Windows/Linux/OS X)
- SpeechRecognition 3.8.1
- pyttsx3 2.7
- pypiwin32 223 (Requerido para poder utilizar pyttsx3)

## Instalación

Para poder comenzar a usar EClassifier se debe instalar primero ```textblob``` junto a ```nltk```
```
$ pip install textblob nltk
```
Si ya cuentas con ```textblob``` pero es inferior a la version ```0.8.0``` entonces debes actualizarlo con
```
$ pip install -U textblob nltk
```
Ahora necesitas instalar lo necesario para poder utilizar en NLP y para eso debes instalar las dependencias de ```SpeechRecognition```, 
primero necesitaras un modulo que permita procesar audio ya sea para grabarlo o reproducidrlo para eso necesitarás ```PyAudio``` ya que es 
un modulo multiplataforma.
```
$ pip install PyAudio
```
#### ¿Equipo basado en x64?
Si cuentas con un equipo con una arquitectura que no es x86 necesitaras instalar adicionalmente ```FLAC encoder```
```
$ pip install flacsync --upgrade --use
```

Una vez instaladas las dependencias de ```SpeechRecognition``` se podrá instalar sin ningun problema.
```
$ pip install SpeechRecognition
```

Ya se ha terminado de instalar la mayoria de las dependencias ahora solo resta instalar el TTS para ello se debe escribir en la terminal lo siguiente
```
$ pip install pyttsx3
```
### Solucion a posibles errores:
- **No module named win32com.client**
- **No module named win32**
- **No module named win32api**

Para solucionarlo solo se debe instalar ```pypiwin32```
```
$ pip install pypiwin32
```

## Configuración
Ya se han instalado todas las dependencias pero como ha sido la primera vez que se instala 
```TextBlob``` se necesita  ```NLTK corpora``` el cual es una paquete de 
```nltk```  para configurarlo solo se necesita un simple comando
```
$ python -m textblob.download_corpora
```
Una vez instalado y configurado se puede utilizar EClassifier.
